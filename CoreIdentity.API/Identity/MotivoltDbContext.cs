﻿using CoreIdentity.Data.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace CoreIdentity.API.Identity
{
    public class MotivoltDbContext : IdentityDbContext<ApplicationUser>
    {
        public MotivoltDbContext(DbContextOptions<MotivoltDbContext> options)   
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
